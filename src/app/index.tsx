import * as React from 'react';
import { hot } from 'react-hot-loader';
import { Route, Switch } from 'react-router';
import { HashRouter } from 'react-router-dom';
import './main.scss';
import {Root} from "app/containers/Root";
import {Index} from "app/containers/Index";

class AppMainContainer extends React.Component<{history: any}, {}> {
    render() {
        return (
            <Root>
                <HashRouter>
                    <Switch>
                        <Route exact path={"/"} component={Index} />
                    </Switch>
                </HashRouter>
            </Root>
        )
    }
}

export const App = hot(module)(AppMainContainer);
